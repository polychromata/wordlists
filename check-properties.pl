#!/usr/bin/env perl
use strict;
use warnings;

my $DIE_SIDE_COUNT = 6;

sub test_word_length {
    my ($words) = @_;
    return [grep { length($_) < 3 || 10 < length($_) } @$words];
}

sub test_no_starts_with {
    my ($words) = @_;
    my %failures = ();
    for (my $i = 0; $i < @$words; ++$i) {
        my $prefix = $words->[$i];
        for (my $j = $i + 1; $j < @$words; ++$j) {
            my $word = $words->[$j];
            last if (rindex($word, $prefix, 0) != 0);
            push(@{$failures{$prefix}}, $word);
        }
    }
    return [map { "$_: @{[join(q{, }, @{$failures{$_}})]}" } sort(keys(%failures))];
}

sub test_unique_prefixes {
    my ($words, $prefix_length) = @_;
    my %failures = ();
    for (my $i = 0; $i < @$words; ++$i) {
        my $word = $words->[$i];
        my $prefix = substr($word, 0, $prefix_length);
        for (++$i; $i < @$words; ++$i) {
            my $next_word = $words->[$i];
            my $next_prefix = substr($next_word, 0, $prefix_length);
            last if $prefix ne $next_prefix;
            push(@{$failures{$prefix}}, $word) unless $failures{$prefix};
            push(@{$failures{$prefix}}, $next_word);
        }
    }
    return [map { "$_: @{[join(q{, }, @{$failures{$_}})]}" } sort(keys(%failures))];
}

sub test_is_lower_alpha {
    my ($words) = @_;
    return [grep { $_ !~ m/\A[a-z]*\z/ } @$words];
}

sub test_list_length {
    my ($words, $die_count) = @_;
    my $expected_length = $DIE_SIDE_COUNT ** $die_count;
    if (@$words != $expected_length) {
        return [
            "expected $DIE_SIDE_COUNT ** $die_count == $expected_length words",
            "but got @{[scalar(@$words)]}",
        ];
    }
    return [];
}

sub test_sorted {
    my ($words) = @_;
    my @failures = ();
    for (my $i = 0; $i < @$words - 1; ++$i) {
        if ($words->[$i] gt $words->[$i + 1]) {
            push(@failures, "$words->[$i] followed by $words->[$i + 1]");
        }
    }
    return \@failures;
}

my @TESTS = (
    {
        "test" => "test_word_length",
        "description" => "word length is between 3 and 10",
        "needs_sorted" => 0,
    },
    {
        "test" => "test_no_starts_with",
        "description" => "no word is a prefix of another word",
        "needs_sorted" => 1,
    },
    {
        "test" => "test_unique_prefixes",
        "description" => "no word has the same prefix as another word",
        "condition" => sub { my %args = @_; $args{"file_name"} =~ /-uniq-([^-])+/ },
        "needs_sorted" => 1,
    },
    {
        "test" => "test_is_lower_alpha",
        "description" => "words are all letters and all lowercase",
        "needs_sorted" => 0,
    },
    {
        "test" => "test_list_length",
        "description" => "the list has the correct length",
        "condition" => sub { my %args = @_; $args{"file_name"} =~ /^words-([^-])+/ },
        "needs_sorted" => 0,
    },
    {
        "test" => "test_sorted",
        "description" => "the list is sorted",
        "needs_sorted" => 0,
    },
);

STDOUT->autoflush(1);
my $test_count = 0;
my $file_count = 0;
my $failing_test_count = 0;
my $failing_file_count = 0;
for my $file_name (@ARGV) {
    ++$file_count;
    my $file;
    unless(open($file, "<:encoding(ascii)", $file_name)) {
        ++$failing_file_count;
        warn "couldn't open file to be tested $file_name: $!";
        next;
    }
    chomp(my @words = <$file>);
    my @words_sorted = sort(@words);
    my @failing_tests = ();
    my %failing_tests = ();
    print("checking $file_name:");

    for my $test (@TESTS) {
        my @args = ();
        if ($test->{"condition"}) {
            @args = $test->{"condition"}->(
                "file_name" => $file_name,
            );
            next unless @args;
        }
        my $failures = (\&{$test->{"test"}})->(
            $test->{"needs_sorted"} ? \@words_sorted : \@words,
            @args,
        );
        ++$test_count;
        if (@$failures) {
            ++$failing_test_count;
            ++$failing_file_count if !@failing_tests;
            push(@failing_tests, $test);
            $failing_tests{$test->{"test"}} = $failures;
        } else {
            print(" $test->{q{test}},");
        }
    }
    print("\n");
    for my $test (@failing_tests) {
        print("\t$test->{q{test}} ($test->{q{description}}) FAILED:\n");
        print(map { "\t\t$_\n" } @{$failing_tests{$test->{"test"}}});
    }
    close($file);
}

print("\n");
print("$test_count tests run in $file_count files\n");
if ($failing_test_count) {
    print("$failing_test_count tests FAILED across $failing_file_count files\n");
    exit(1);
} else {
    print("all tests passed\n");
}
